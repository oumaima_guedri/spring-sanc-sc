package edu.iset.salledesport.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.iset.salledesport.entities.ERole;
import edu.iset.salledesport.entities.Role;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

	  Role findByName(ERole name);
}
