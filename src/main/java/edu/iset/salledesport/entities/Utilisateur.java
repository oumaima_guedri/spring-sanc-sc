package edu.iset.salledesport.entities;
import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;



@Entity
public class Utilisateur implements Serializable {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;
	private String nom;
	private String prenom;
	private Date datenaiss;
	private String email;
	private String password;
	private int telephone;
	private int age;
	private String typesport;
	private double poids;
	private double taille;
	private String presence;
	
	
	
	@ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE},fetch = FetchType.EAGER)
	  private Set<Role> roles = new HashSet<>();
	
  
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public Utilisateur () {
		
	}
	
	
	
	public Utilisateur( String prenom ,String email, String password
			) {
		
		
		this.prenom = prenom;
		this.email = email;
		this.password = password;
		
	}
	/*public Utilisateur(String prenom, String email, String password) {
		 this.prenom = prenom;
		    this.email = email;
		    this.password = password;
	}*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getTelephone() {
		return telephone;
	}
	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getTypesport() {
		return typesport;
	}
	public void setTypesport(String typesport) {
		this.typesport = typesport;
	}
	public double getPoids() {
		return poids;
	}
	public void setPoids(double poids) {
		this.poids = poids;
	}
	public double getTaille() {
		return taille;
	}
	public void setTaille(double taille) {
		this.taille = taille;
	}
	public String getPresence() {
		return presence;
	}
	public void setPresence(String presence) {
		this.presence = presence;
	}

	public Date getDatenaiss() {
		return datenaiss;
	}

	public void setDatenaiss(Date datenaiss) {
		this.datenaiss = datenaiss;
	}
	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", datenaiss=" + datenaiss + ", email="
				+ email + ", password=" + password + ", telephone=" + telephone + ", age=" + age + ", typesport="
				+ typesport + ", poids=" + poids + ", taille=" + taille + ", presence=" + presence + ", role=" + roles
				+ "]";
	}
	
	
	
	

}
